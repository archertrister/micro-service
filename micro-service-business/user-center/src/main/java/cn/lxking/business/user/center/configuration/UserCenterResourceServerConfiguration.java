package cn.lxking.business.user.center.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
//import org.springframework.security.oauth2.provider.token.TokenStore;
//import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;
//import org.springframework.boot.context.properties.ConfigurationProperties;
//import org.springframework.boot.jdbc.DataSourceBuilder;
//import org.springframework.context.annotation.Bean;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//
//import javax.sql.DataSource;

/**
 * @author ：ArcherTrister
 * @date ：Created in 2020/2/27 14:19
 * @description：用户中心资源服务器配置
 * @modified By：
 * @version: 1.0.0
 */
@Configuration
@EnableResourceServer
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true, jsr250Enabled = true)
public class UserCenterResourceServerConfiguration extends ResourceServerConfigurerAdapter {
    // TODO: 没搞懂tokenStore有啥用
//    @Bean
//    public BCryptPasswordEncoder passwordEncoder() {
//        return new BCryptPasswordEncoder();
//    }

//    @Bean
//    @ConfigurationProperties(prefix="spring.datasource")
//    public DataSource ouathDataSource(){return DataSourceBuilder.create().build();}
//
//    @Override
//    public void configure(ResourceServerSecurityConfigurer resources)throws Exception {
//        // 配置资源 ID
//        TokenStore tokenStore=new JdbcTokenStore(ouathDataSource());
//        resources.resourceId("user-center-resources").tokenStore(tokenStore);
//    }

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) {
        // 配置资源 ID
        resources.resourceId("user-center-resources");
//resourceId 用于分配给可授予的clientId
//stateless  标记以指示在这些资源上仅允许基于令牌的身份验证
//tokenStore token的存储方式（上一章节提到）
//authenticationEntryPoint  认证异常流程处理返回
//tokenExtractor            token获取方式,默认BearerTokenExtractor
//从header获取token为空则从request.getParameter("access_token")
//        resources.resourceId(RESOURCE_ID).stateless(true).tokenStore(tokenStore)
//             .authenticationEntryPoint(authenticationEntryPoint).tokenExtractor(unicomTokenExtractor);
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
                .exceptionHandling()
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
                .and()
                .authorizeRequests()
                .antMatchers("/**").hasAuthority("USER");
    }
}
