package cn.lxking.business.user.center.controller;

import cn.lxking.provider.dubbo.base.client.service.EchoService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ArcherTrister
 */
@RestController
@RequestMapping(value = "echo")
public class EchoController {

  @Reference(version = "1.0.0")
  private EchoService echoService;

  @GetMapping(value = "{message}")
  public String echo(@PathVariable String message) {
    return echoService.echo(message);
  }
}
