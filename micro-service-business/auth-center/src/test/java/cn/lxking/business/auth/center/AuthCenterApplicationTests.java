package cn.lxking.business.auth.center;

import cn.lxking.business.auth.center.utils.Utils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AuthCenterApplicationTests {

	@Test
	public void contextLoads() {
	}

	@Test
	public void passwardTest(){
		System.out.println(Utils.passwordEncoder("123456"));
		System.out.println(Utils.passwordEncoder("secret"));
	}

}
