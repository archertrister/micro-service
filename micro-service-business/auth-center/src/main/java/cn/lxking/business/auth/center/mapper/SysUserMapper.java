package cn.lxking.business.auth.center.mapper;

import cn.lxking.business.auth.center.domain.SysUser;
import cn.lxking.core.mapper.BaseMapper;

/**
 * @author     ：ArcherTrister
 * @date       ：Created in 2020/2/26 16:18
 * @description：${description}
 * @modified By：
 * @version:     $version
 */
public interface SysUserMapper extends BaseMapper<SysUser> {
}