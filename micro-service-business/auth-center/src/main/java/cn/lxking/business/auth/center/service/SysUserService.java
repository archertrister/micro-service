package cn.lxking.business.auth.center.service;

import cn.lxking.business.auth.center.domain.SysUser;
import cn.lxking.business.auth.center.dto.RegisterRequest;

/**
 * @author     ：ArcherTrister
 * @date       ：Created in 2020/2/26 16:18
 * @description：${description}
 * @modified By：
 * @version:     $version
 */
public interface SysUserService{
    /**
     * 新增用户
     *
     * @param registerRequest {@link RegisterRequest}
     * @return {@code int} 大于 0 则表示添加成功
     */
    int addUser(RegisterRequest registerRequest);

    /**
     * 获取用户
     *
     * @param username 用户名
     * @return {@link SysUser}
     */
    SysUser getUserByName(String username);

    /**
     * 获取用户
     *
     * @param sysUser {@link SysUser}
     * @return {@link SysUser}
     */
    SysUser getUserByUser(SysUser sysUser);

    /**
     * 更新用户
     * <p>
     * 仅允许更新 邮箱、昵称、备注、状态
     * </p>
     *
     * @param sysUser {@link SysUser}
     * @return {@code int} 大于 0 则表示更新成功
     */
    int updateUser(SysUser sysUser);

    /**
     * 修改密码
     *
     * @param username {@code String} 用户名
     * @param password {@code String} 明文密码
     * @return {@code int} 大于 0 则表示更新成功
     */
    int modifyPassword(String username, String password);

    /**
     * 修改头像
     *
     * @param username {@code String} 用户名
     * @param path     {@code String} 头像地址
     * @return {@code int} 大于 0 则表示更新成功
     */
    int modifyAvatar(String username, String path);

}
