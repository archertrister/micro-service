package cn.lxking.business.auth.center.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author ：ArcherTrister
 * @date ：Created in 2020/2/24 14:53
 * @description：登录信息
 * @modified By：
 * @version: 1.0.0
 */
@Data
public class LoginInfo implements Serializable {
    private List<String> roles;
    private String name;
    private String avatar;
    private String introduction;
}
