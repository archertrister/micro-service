package cn.lxking.business.auth.center.service.impl;

import cn.lxking.business.auth.center.domain.SysUser;
import cn.lxking.business.auth.center.service.SysUserService;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ：ArcherTrister
 * @version : 1.0.0
 * @date ：Created in 2020-03-01 19:31
 * @description ：用户详情服务实现
 * @modified By ：
 */
public class UserDetailsServiceImpl implements UserDetailsService {
    @Resource
    private SysUserService sysUserService;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        // 查询用户
        SysUser user = sysUserService.getUserByName(userName);

        if(user==null){

            throw new UsernameNotFoundException("User"+userName+"can not be found");
        }
        // 默认所有用户拥有 USER 权限
        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        GrantedAuthority grantedAuthority = new SimpleGrantedAuthority("ROLE_USER");
        grantedAuthorities.add(grantedAuthority);
        //return new User(user.getUsername(),user.getPassword(), grantedAuthorities);
        return new User(user.getUsername(),user.getPassword(),true,true,true,true,grantedAuthorities);
    }
}
