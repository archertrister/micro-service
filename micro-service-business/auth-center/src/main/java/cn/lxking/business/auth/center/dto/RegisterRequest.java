package cn.lxking.business.auth.center.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author ：ArcherTrister
 * @date ：Created in 2020/2/27 10:33
 * @description：注册请求模型
 * @modified By：
 * @version: 1.0.0
 */
@Data
public class RegisterRequest implements Serializable {
    private String username;
    private String password;
}
