package cn.lxking.business.auth.center.service.impl;

import cn.lxking.business.auth.center.domain.SysUser;
import cn.lxking.business.auth.center.dto.RegisterRequest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import cn.lxking.business.auth.center.mapper.SysUserMapper;
import cn.lxking.business.auth.center.service.SysUserService;
import tk.mybatis.mapper.entity.Example;

import java.util.Date;

/**
 * @author     ：ArcherTrister
 * @date       ：Created in 2020/2/26 16:18
 * @description：${description}
 * @modified By：
 * @version:     $version
 */
@Service
public class SysUserServiceImpl implements SysUserService {

    @Resource
    private SysUserMapper sysUserMapper;

    @Resource
    private BCryptPasswordEncoder passwordEncoder;

    @Override
    public int addUser(RegisterRequest registerRequest) {
        SysUser user = new SysUser();
        user.setUsername(registerRequest.getUsername());
        user.setPassword(passwordEncoder.encode(registerRequest.getPassword()));
//        user.setPhone();
//        user.setEmail();
        user.setCreated(new Date());
        user.setUpdated(new Date());
        return sysUserMapper.insert(user);
    }

    @Override
    public SysUser getUserByName(String username) {
        Example example = new Example(SysUser.class);
        example.createCriteria().andEqualTo("username", username);
        return sysUserMapper.selectOneByExample(example);
    }

    @Override
    public SysUser getUserByUser(SysUser sysUser) {
        return null;
    }

    @Override
    public int updateUser(SysUser sysUser) {
        return 0;
    }

    @Override
    public int modifyPassword(String username, String password) {
        return 0;
    }

    @Override
    public int modifyAvatar(String username, String path) {
        return 0;
    }
}

