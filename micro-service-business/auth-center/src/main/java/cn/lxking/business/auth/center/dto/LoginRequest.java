package cn.lxking.business.auth.center.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author ：ArcherTrister
 * @date ：Created in 2020/2/24 14:53
 * @description：登录请求参数
 * */
@Data
public class LoginRequest implements Serializable {
    private String username;
    private String password;
}
