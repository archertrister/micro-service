package cn.lxking.business.auth.center;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * @author ArcherTrister
 */
@SpringBootApplication
@MapperScan(basePackages = "cn.lxking.business.auth.center.mapper")
public class AuthCenterApplication {
	public static void main(String[] args) {
		SpringApplication.run(AuthCenterApplication.class, args);
	}
}
