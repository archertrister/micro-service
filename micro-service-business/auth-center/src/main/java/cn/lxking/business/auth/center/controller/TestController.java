package cn.lxking.business.auth.center.controller;

import cn.lxking.core.data.ResponseResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ：ArcherTrister
 * @version : 1.0.0
 * @date ：Created in 2020/3/2 11:27
 * @description ：测试接口
 * @modified By ：
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("test")
public class TestController {
    /**
     * 测试
     *
     * @return {@link ResponseResult}
     */
    @GetMapping("/get")
    public ResponseResult test()
    {
        return new ResponseResult<>(ResponseResult.CodeStatus.OK, "Get测试成功", null);
    }

    /**
     * 测试
     *
     * @return {@link ResponseResult}
     */
    @PostMapping("/post")
    public ResponseResult postTest()
    {
        return new ResponseResult<>(ResponseResult.CodeStatus.OK, "Post测试成功", null);
    }


    /**
     * 测试
     *
     * @return {@link ResponseResult}
     */
    @GetMapping("/noLogin")
    public ResponseResult noLogin()
    {
        return new ResponseResult<>(ResponseResult.CodeStatus.OK, "NoLogin测试成功", null);
    }

}
