package cn.lxking.business.auth.center.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

/**
 * @author ：ArcherTrister
 * @version : 1.0.0
 * @date ：Created in 2020-03-01 19:30
 * @description ：资源配置
 * @modified By ：
 */
@Configuration
//@Order(6)
@EnableResourceServer
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

    private static final String  RESOURCE_ID = "authorizationResourceApi";

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) {
        resources.resourceId(RESOURCE_ID); //.stateless(false);
    }

    @Override
    public void configure(HttpSecurity http) throws Exception{
        http
//                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
//                .and()
//                .requestMatchers()
////                .antMatchers("/vue-element-admin/user/**")
//                .and()

                .authorizeRequests()
                .antMatchers("/login","/logout.do").permitAll()
                .antMatchers("/").permitAll()
                .antMatchers("/vue-element-admin/user/login").permitAll()
                .antMatchers("/test/noLogin").permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginProcessingUrl("/login.do")
                .usernameParameter("username")
                .passwordParameter("password")
                .loginPage("/login")
                .and()
                .logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout.do")).permitAll();

                //.formLogin().permitAll();
    }

//    @Override
//    public void configure(HttpSecurity http) throws Exception{
//        http
////                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
////                .and()
////                .requestMatchers()
////                .antMatchers("/" , "/login", "/oauth/authorize")
////                .and()
//                .authorizeRequests()
//                //.antMatchers("/").permitAll()
//                //.antMatchers(HttpMethod.GET,"/oauth/authorize").permitAll()
//                //.antMatchers("/login").permitAll()
//                .antMatchers("/vue-element-admin/user/login").permitAll()
////                .antMatchers("/vue-element-admin/user/info").hasAuthority("ROLE_USER")
////                .antMatchers("/vue-element-admin/user/logout").hasAuthority("ROLE_USER")
//                .anyRequest().authenticated();
//  }
}
