package cn.lxking.core.social.qq.api;

/**
 * @author ：ArcherTrister
 * @version :
 * @date ：Created in 2020/3/3 11:29
 * @description ：
 * @modified By ：
 */
public interface QQ {
    QQUserProfile getUserInfo();
}
