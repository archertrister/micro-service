package cn.lxking.core.social.qq.config;

import cn.lxking.core.social.qq.connet.QQConnectionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.social.config.annotation.ConnectionFactoryConfigurer;
import org.springframework.social.config.annotation.SocialConfigurerAdapter;
import org.springframework.social.connect.ConnectionFactory;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.UsersConnectionRepository;

/**
 * @author ：ArcherTrister
 * @version :
 * @date ：Created in 2020/3/3 13:28
 * @description ：
 * @modified By ：
 */
@Configuration
@ConditionalOnProperty(prefix = "security.oauth2.social.qq", name = "appId")
public class QQAutoConfig extends SocialConfigurerAdapter {

    /**
     * provider id.
     */
    private String providerId = "qq";

    /**
     * Application id.
     */
    @Value("${security.oauth2.social.qq.appId}")
    private String appId;

    /**
     * Application secret.
     */
    @Value("${security.oauth2.social.qq.appSecret}")
    private String appSecret;

    @Override
    public void addConnectionFactories(ConnectionFactoryConfigurer configurer,
                                       Environment environment) {
        configurer.addConnectionFactory(createConnectionFactory());
    }

    /**
     * qq连接工厂
     */
    protected ConnectionFactory<?> createConnectionFactory() {
        return new QQConnectionFactory(
                providerId,
                appId,
                appSecret);
    }

    // 后补：做到处理注册逻辑的时候发现的一个bug：登录完成后，数据库没有数据，但是再次登录却不用注册了
    // 就怀疑是否是在内存中存储了。结果果然发现这里父类的内存ConnectionRepository覆盖了SocialConfig中配置的jdbcConnectionRepository
    // 这里需要返回null，否则会返回内存的 ConnectionRepository
    @Override
    public UsersConnectionRepository getUsersConnectionRepository(ConnectionFactoryLocator connectionFactoryLocator) {
        return null;
    }

}
