package cn.lxking.core.social.qq.connet;

import cn.lxking.core.social.qq.api.QQ;
import org.springframework.social.connect.support.OAuth2ConnectionFactory;

/**
 * @author ：ArcherTrister
 * @version :
 * @date ：Created in 2020/3/3 13:35
 * @description ：
 * @modified By ：
 */
public class QQConnectionFactory extends OAuth2ConnectionFactory<QQ> {

    public QQConnectionFactory(String providerId, String appId, String appSecret) {
        super(providerId, new QQServiceProvider(appId, appSecret), new QQAdapter());
    }

}
