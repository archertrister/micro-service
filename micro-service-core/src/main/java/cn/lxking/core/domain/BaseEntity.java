package cn.lxking.core.domain;

import cn.lxking.core.utils.GenIdUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

/**
 * 通用的领域模型
 *
 * <p>Title: AbstractBaseDomain
 *
 * <p>Description:
 *
 * @author ArcherTrister
 * @version 1.0.0
 * @date 2020/02/20 09:25
 */
@Data
public abstract class BaseEntity implements Serializable {
  /**
   * ID
   */
  @Id
  @KeySql(genId = GenIdUtil.class)
  private String id;

  /**
   * 创建时间
   */
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
  private Date created;

  /**
   * 更新时间
   */
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
  private Date updated;
}
