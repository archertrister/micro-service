package cn.lxking.core.configuration;

import cn.lxking.core.interceptor.FeignRequestInterceptor;
import feign.RequestInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author ：ArcherTrister
 * @date ：Created in 2020-02-24 20:44
 * @description：Feign 配置
 * @modified By：
 * @version: 1.0.0
 */
@Configuration
public class FeignRequestConfiguration {
    @Bean
    public RequestInterceptor requestInterceptor() {
        return new FeignRequestInterceptor();
    }
}