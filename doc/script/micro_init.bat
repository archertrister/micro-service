@echo off

call mvn clean install -f ..\..\micro-service-core\pom.xml
call mvn install:install-file -DgroupId=cn.lxking -DartifactId=micro-service-core -Dversion=0.0.1-SNAPSHOT -Dpackaging=jar -Dfile=..\..\micro-service-core\target\micro-service-core-0.0.1-SNAPSHOT.jar

call mvn clean install -f ..\..\micro-service-provider\dubbo-base-client\pom.xml
call mvn install:install-file -DgroupId=cn.lxking -DartifactId=micro-service-provider-dubbo-base-client -Dversion=0.0.1-SNAPSHOT -Dpackaging=jar -Dfile=..\..\micro-service-provider\dubbo-base-client\target\micro-service-provider-dubbo-base-client-0.0.1-SNAPSHOT.jar

call mvn clean install -f ..\..\micro-service-provider\feign-base-client\pom.xml
call mvn install:install-file -DgroupId=cn.lxking -DartifactId=micro-service-provider-feign-base-client -Dversion=0.0.1-SNAPSHOT -Dpackaging=jar -Dfile=..\..\micro-service-provider\feign-base-client\target\micro-service-provider-feign-base-client-0.0.1-SNAPSHOT.jar

call mvn clean install -f ..\..\pom.xml