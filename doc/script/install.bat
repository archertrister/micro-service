@echo off

call mvn clean install -f ..\..\micro-service-core\pom.xml
call mvn install:install-file -DgroupId=cn.lxking -DartifactId=micro-service-core -Dversion=0.0.1-SNAPSHOT -Dpackaging=jar -Dfile=..\..\micro-service-core\target\micro-service-core-0.0.1-SNAPSHOT.jar

call mvn clean install -f ..\..\micro-service-provider\dubbo-base-client\pom.xml
call mvn install:install-file -DgroupId=cn.lxking -DartifactId=micro-service-provider-dubbo-base-client -Dversion=0.0.1-SNAPSHOT -Dpackaging=jar -Dfile=..\..\micro-service-provider\dubbo-base-client\target\micro-service-provider-dubbo-base-client-0.0.1-SNAPSHOT.jar

call mvn clean install -f ..\..\micro-service-provider\feign-base-client\pom.xml
call mvn install:install-file -DgroupId=cn.lxking -DartifactId=micro-service-provider-feign-base-client -Dversion=0.0.1-SNAPSHOT -Dpackaging=jar -Dfile=..\..\micro-service-provider\feign-base-client\target\micro-service-provider-feign-base-client-0.0.1-SNAPSHOT.jar

call mvn clean install -f ..\..\sample\hello-apache-dubbo\hello-apache-dubbo-provider\hello-apache-dubbo-provider-api\pom.xml
call mvn install:install-file -DgroupId=cn.lxking -DartifactId=hello-apache-dubbo-provider-api -Dversion=0.0.1-SNAPSHOT -Dpackaging=jar -Dfile=..\..\sample\hello-apache-dubbo\hello-apache-dubbo-provider\hello-apache-dubbo-provider-api\target\hello-apache-dubbo-provider-api-0.0.1-SNAPSHOT.jar

call mvn clean install -f ..\..\sample\hello-spring-security-oauth2\hello-spring-security-oauth2-core\pom.xml
call mvn install:install-file -DgroupId=cn.lxking -DartifactId=hello-spring-security-oauth2-core -Dversion=0.0.1-SNAPSHOT -Dpackaging=jar -Dfile=..\..\sample\hello-spring-security-oauth2\hello-spring-security-oauth2-core\target\hello-spring-security-oauth2-core-0.0.1-SNAPSHOT.jar

