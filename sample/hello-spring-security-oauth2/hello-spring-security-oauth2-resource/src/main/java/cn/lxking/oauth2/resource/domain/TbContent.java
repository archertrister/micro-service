package cn.lxking.oauth2.resource.domain;

import cn.lxking.oauth2.core.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = true)
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "micro_service_sample.tb_content")
public class TbContent extends BaseEntity implements Serializable {
  private static final long serialVersionUID = 1L;
  /** 内容类目ID */
  @Column(name = "category_id")
  private String categoryId;
  /** 内容标题 */
  @Column(name = "title")
  private String title;
  /** 子标题 */
  @Column(name = "sub_title")
  private String subTitle;
  /** 标题描述 */
  @Column(name = "title_desc")
  private String titleDesc;
  /** 链接 */
  @Column(name = "url")
  private String url;
  /** 图片绝对路径 */
  @Column(name = "pic")
  private String pic;
  /** 图片2 */
  @Column(name = "pic2")
  private String pic2;
  /** 内容 */
  @Column(name = "content")
  private String content;
}
