package cn.lxking.oauth2.resource.mapper;

import cn.lxking.oauth2.core.mapper.BaseMapper;
import cn.lxking.oauth2.resource.domain.TbContent;

public interface TbContentMapper extends BaseMapper<TbContent> {}
