package cn.lxking.oauth2.server.mapper;

import cn.lxking.oauth2.core.mapper.BaseMapper;
import cn.lxking.oauth2.server.domain.BasePermission;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BasePermissionMapper extends BaseMapper<BasePermission> {
  List<BasePermission> selectByUserId(@Param("userId") String userId);
}
