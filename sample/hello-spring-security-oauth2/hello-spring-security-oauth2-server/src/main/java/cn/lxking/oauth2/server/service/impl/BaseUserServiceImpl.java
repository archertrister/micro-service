package cn.lxking.oauth2.server.service.impl;

import cn.lxking.oauth2.server.domain.BaseUser;
import cn.lxking.oauth2.server.mapper.BaseUserMapper;
import cn.lxking.oauth2.server.service.BaseUserService;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;

@Service
public class BaseUserServiceImpl implements BaseUserService {

  @Resource private BaseUserMapper baseUserMapper;

  @Override
  public BaseUser getByUsername(String username) {
    Example example = new Example(BaseUser.class);
    example.createCriteria().andEqualTo("username", username);
    return baseUserMapper.selectOneByExample(example);
  }
}
