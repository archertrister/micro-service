package cn.lxking.oauth2.server.mapper;

import cn.lxking.oauth2.server.domain.BaseRole;
import tk.mybatis.mapper.common.Mapper;

public interface BaseRoleMapper extends Mapper<BaseRole> {}
