package cn.lxking.oauth2.server.domain;

import cn.lxking.oauth2.core.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = true)
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "micro_service_sample.base_user")
public class BaseUser extends BaseEntity implements Serializable {
  private static final long serialVersionUID = 1L;
  /** 用户名 */
  @Column(name = "username")
  private String username;
  /** 密码，加密存储 */
  @Column(name = "`password`")
  private String password;
  /** 注册手机号 */
  @Column(name = "phone")
  private String phone;
  /** 注册邮箱 */
  @Column(name = "email")
  private String email;
}
