package cn.lxking.oauth2.server.service.impl;

import cn.lxking.oauth2.server.mapper.BaseRoleMapper;
import cn.lxking.oauth2.server.service.BaseRoleService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class BaseRoleServiceImpl implements BaseRoleService {

  @Resource private BaseRoleMapper baseRoleMapper;
}
