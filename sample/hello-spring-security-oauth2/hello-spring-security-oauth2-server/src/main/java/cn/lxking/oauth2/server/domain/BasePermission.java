package cn.lxking.oauth2.server.domain;

import cn.lxking.oauth2.core.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = true)
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "micro_service_sample.base_permission")
public class BasePermission extends BaseEntity implements Serializable {
  private static final long serialVersionUID = 1L;
  /** 父权限 */
  @Column(name = "parent_id")
  private String parentId;
  /** 权限名称 */
  @Column(name = "`name`")
  private String name;
  /** 权限英文名称 */
  @Column(name = "enname")
  private String enname;
  /** 授权路径 */
  @Column(name = "url")
  private String url;
  /** 备注 */
  @Column(name = "description")
  private String description;
}
