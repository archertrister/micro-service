package cn.lxking.oauth2.server.service;

import cn.lxking.oauth2.server.domain.BaseUser;

public interface BaseUserService {
  default BaseUser getByUsername(String username) {
    return null;
  }
}
