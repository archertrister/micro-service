package cn.lxking.oauth2.server.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @author ArcherTrister
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true, jsr250Enabled = true)
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {
  @Bean
  public BCryptPasswordEncoder passwordEncoder() {
    // 配置默认的加密方式
    return new BCryptPasswordEncoder();
  }

  /** 内存配置 */
  //    @Override
  //    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
  //        // 在内存中创建用户
  //        auth.inMemoryAuthentication()
  //                .withUser("test").password(passwordEncoder().encode("123456")).roles("USER")
  //                .and()
  //                .withUser("admin").password(passwordEncoder().encode("123456")).roles("ADMIN");
  //    }

  /** JDBC 配置 */
  @Bean
  @Override
  protected UserDetailsService userDetailsService() {
    return new UserDetailsServiceImpl();
  }

  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    // 使用自定义认证与授权
    auth.userDetailsService(userDetailsService());
  }

  @Override
  public void configure(WebSecurity web) throws Exception {
    // 将 check_token 暴露出去，否则资源服务器访问时报 403 错误
    web.ignoring().antMatchers("/oauth/check_token");
  }
}
