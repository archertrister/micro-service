package cn.lxking.oauth2.server.mapper;

import cn.lxking.oauth2.core.mapper.BaseMapper;
import cn.lxking.oauth2.server.domain.BaseUser;

public interface BaseUserMapper extends BaseMapper<BaseUser> {}
