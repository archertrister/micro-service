package cn.lxking.oauth2.server.service.impl;

import cn.lxking.oauth2.server.domain.BasePermission;
import cn.lxking.oauth2.server.mapper.BasePermissionMapper;
import cn.lxking.oauth2.server.service.BasePermissionService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class BasePermissionServiceImpl implements BasePermissionService {

  @Resource private BasePermissionMapper basePermissionMapper;

  @Override
  public List<BasePermission> selectByUserId(String userId) {
    return basePermissionMapper.selectByUserId(userId);
  }
}
