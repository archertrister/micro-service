package cn.lxking.oauth2.server.domain;

import cn.lxking.oauth2.core.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = true)
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "micro_service_sample.base_role")
public class BaseRole extends BaseEntity implements Serializable {
  private static final long serialVersionUID = 1L;
  /** 父角色 */
  @Column(name = "parent_id")
  private String parentId;
  /** 角色名称 */
  @Column(name = "`name`")
  private String name;
  /** 角色英文名称 */
  @Column(name = "enname")
  private String enname;
  /** 备注 */
  @Column(name = "description")
  private String description;
}
