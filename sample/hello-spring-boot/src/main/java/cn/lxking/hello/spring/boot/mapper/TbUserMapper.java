package cn.lxking.hello.spring.boot.mapper;

import cn.lxking.hello.spring.boot.domain.TbUser;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface TbUserMapper extends Mapper<TbUser> {
  int updateBatch(List<TbUser> list);

  int updateBatchSelective(List<TbUser> list);

  int batchInsert(@Param("list") List<TbUser> list);

  int insertOrUpdate(TbUser record);

  int insertOrUpdateSelective(TbUser record);
}
