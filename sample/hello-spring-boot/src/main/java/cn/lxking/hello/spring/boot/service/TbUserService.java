package cn.lxking.hello.spring.boot.service;

import cn.lxking.hello.spring.boot.domain.TbUser;

import java.util.List;

public interface TbUserService {

  int updateBatch(List<TbUser> list);

  int updateBatchSelective(List<TbUser> list);

  int batchInsert(List<TbUser> list);

  int insertOrUpdate(TbUser record);

  int insertOrUpdateSelective(TbUser record);
}
