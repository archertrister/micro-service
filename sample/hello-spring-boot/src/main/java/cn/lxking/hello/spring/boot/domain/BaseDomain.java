package cn.lxking.hello.spring.boot.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import cn.lxking.hello.utils.UUIdGenId;
import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import java.io.Serializable;
import java.sql.Date;

/**
 * 通用的领域模型
 *
 * <p>Title: AbstractBaseDomain
 *
 * <p>Description:
 *
 * @author ArcherTrister
 * @version 1.0.0
 * @date 2020/02/20 09:25
 */
@Data
public abstract class BaseDomain implements Serializable {
  /** 该注解需要保留，用于 tk.mybatis 回显 ID */
  @Id
  @KeySql(genId = UUIdGenId.class)
  private String id;

  /** 格式化日期，由于是北京时间（我们是在东八区），所以时区 +8 */
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
  private Date created;

  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
  private Date updated;
}
