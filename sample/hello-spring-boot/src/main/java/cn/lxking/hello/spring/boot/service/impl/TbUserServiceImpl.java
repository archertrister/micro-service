package cn.lxking.hello.spring.boot.service.impl;

import cn.lxking.hello.spring.boot.domain.TbUser;
import cn.lxking.hello.spring.boot.mapper.TbUserMapper;
import cn.lxking.hello.spring.boot.service.TbUserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class TbUserServiceImpl implements TbUserService {

  @Resource private TbUserMapper tbUserMapper;

  @Override
  public int updateBatch(List<TbUser> list) {
    return tbUserMapper.updateBatch(list);
  }

  @Override
  public int updateBatchSelective(List<TbUser> list) {
    return tbUserMapper.updateBatchSelective(list);
  }

  @Override
  public int batchInsert(List<TbUser> list) {
    return tbUserMapper.batchInsert(list);
  }

  @Override
  public int insertOrUpdate(TbUser record) {
    return tbUserMapper.insertOrUpdate(record);
  }

  @Override
  public int insertOrUpdateSelective(TbUser record) {
    return tbUserMapper.insertOrUpdateSelective(record);
  }
}
