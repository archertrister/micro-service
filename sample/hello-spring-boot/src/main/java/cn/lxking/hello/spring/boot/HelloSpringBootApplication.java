package cn.lxking.hello.spring.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * @author ArcherTrister
 */
@SpringBootApplication
@MapperScan(basePackages = "cn.lxking.hello.spring.boot.mapper")
public class HelloSpringBootApplication {

  public static void main(String[] args) {
    SpringApplication.run(HelloSpringBootApplication.class, args);
  }
}
