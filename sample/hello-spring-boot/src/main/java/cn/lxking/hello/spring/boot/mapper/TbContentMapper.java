package cn.lxking.hello.spring.boot.mapper;

import cn.lxking.hello.spring.boot.domain.TbContent;
import cn.lxking.hello.utils.mapper.BaseMapper;

public interface TbContentMapper extends BaseMapper<TbContent> {}
