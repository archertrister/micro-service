package cn.lxking.hello.utils;

import tk.mybatis.mapper.genid.GenId;

import java.util.UUID;

/**
 * @author ArcherTrister
 * @version 1.0.0
 * @date on 2020/02/20 09:25
 * @description V1.0
 */
public class UUIdGenId implements GenId<String> {
  @Override
  public String genId(String s, String s1) {
    return UUID.randomUUID().toString().replace("-", "");
  }
}
