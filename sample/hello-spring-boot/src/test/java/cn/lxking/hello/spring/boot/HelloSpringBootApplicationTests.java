package cn.lxking.hello.spring.boot;

import cn.lxking.hello.spring.boot.domain.TbUser;
import cn.lxking.hello.spring.boot.mapper.TbUserMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@Rollback
class HelloSpringBootApplicationTests {

  @Resource private TbUserMapper tbUserMapper;

  @Test
  public void testPrintColors() {
    System.out.println(" \033[(前缀),m(后缀),格式:\033[XX;XX;XXm");
    System.out.println("------ 字体颜色30~37 ------");
    System.out.println("\033[30m" + "就是酱紫的");
    System.out.println("\033[31m" + "就是酱紫的");
    System.out.println("\033[32m" + "就是酱紫的");
    System.out.println("\033[37m" + "就是酱紫的");
    System.out.println("------ 背景颜色40~47 -------");
    System.out.println("\033[43m" + "就是酱紫的");
    System.out.println("\033[44m" + "就是酱紫的" + "\033[m");
    System.out.println("\033[45m" + "就是酱紫的");
    System.out.println("\033[46m" + "就是酱紫的" + "\033[m");
    System.out.println("--- 1:加粗,;:隔开,90~97字体颜色变亮 ---");
    System.out.println("\033[1;90m" + "就是酱紫的");
    System.out.println("\033[1;94m" + "就是酱紫的");
    System.out.println("\033[1;95m" + "就是酱紫的");
    System.out.println("\033[1;97m" + "就是酱紫的");
    System.out.println("\033[1;93;45m" + "就是酱紫的" + "\033[m");
  }

  @Test
  public void testSelectAll() {
    List<TbUser> tbUsers = tbUserMapper.selectAll();
    tbUsers.forEach(
        tbUser -> {
          System.out.println("\033[46m" + tbUser + "\033[m");
        });
  }

  @Test
  public void testInsert() {
    TbUser tbUser = new TbUser();
    tbUser.setUsername("3");
    tbUser.setPassword("3");
    tbUser.setPhone("3");
    tbUser.setEmail("3");
    tbUser.setId("3");
    tbUserMapper.insert(tbUser);
  }

  @Test
  public void testPage() {
    PageHelper.startPage(1, 10);
    PageInfo<TbUser> pageInfo = new PageInfo<>(tbUserMapper.selectAll());
    System.out.println("\033[46m" + pageInfo + "\033[m");
    System.out.println("\033[46m" + pageInfo.getTotal() + "\033[m");
    System.out.println("\033[46m" + pageInfo.getPages() + "\033[m");
    pageInfo
        .getList()
        .forEach(
            tbUser -> {
              System.out.println("\033[46m" + tbUser + "\033[m");
            });
  }
}
