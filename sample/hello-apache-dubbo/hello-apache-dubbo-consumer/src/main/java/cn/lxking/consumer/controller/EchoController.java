package cn.lxking.consumer.controller;

import cn.lxking.provider.api.EchoService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ArcherTrister
 */
@RefreshScope
@RestController
public class EchoController {

  @Reference(version = "1.0.0")
  private EchoService echoService;

  @Value("${user.name}")
  private String userName;

  @GetMapping(value = "/echo/{string}")
  public String echo(@PathVariable String string) {
    return echoService.echo(string);
  }

  @GetMapping(value = "/lb")
  public String lb() {
    return echoService.lb();
  }

  @GetMapping(value = "/config")
  public String config() {
    return echoService.echo(userName);
  }
}
