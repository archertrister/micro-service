package cn.lxking.consumer.service.fallback;

import cn.lxking.consumer.service.EchoService;
import org.springframework.stereotype.Component;

/** @author ArcherTrister */
@Component
public class EchoServiceFallbackImpl implements EchoService {

  @Override
  public String echo(String string) {
    return "你的网络有问题";
  }

  @Override
  public String lb() {
    return "请联系管理员";
  }
}
