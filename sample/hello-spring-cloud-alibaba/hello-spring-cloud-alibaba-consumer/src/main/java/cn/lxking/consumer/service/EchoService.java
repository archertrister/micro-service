package cn.lxking.consumer.service;

import cn.lxking.consumer.service.fallback.EchoServiceFallbackImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "service-provider", fallback = EchoServiceFallbackImpl.class)
public interface EchoService {
  @GetMapping(value = "/echo/{string}")
  String echo(@PathVariable("string") String string);

  @GetMapping(value = "/lb")
  String lb();
}
