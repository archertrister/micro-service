package cn.lxking.consumer.controller;

import cn.lxking.consumer.service.EchoService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RefreshScope
@RestController
public class FeignTestController {

  @Value("${user.name}")
  private String userName;

  @Resource private EchoService echoService;

  @GetMapping(value = "/feign/echo/{str}")
  public String echo(@PathVariable String str) {
    return echoService.echo(str);
  }

  @GetMapping(value = "/feign/lb")
  public String lb() {
    return echoService.lb();
  }

  @GetMapping(value = "/feign/config")
  public String config() {
    return echoService.echo(userName);
  }
}
