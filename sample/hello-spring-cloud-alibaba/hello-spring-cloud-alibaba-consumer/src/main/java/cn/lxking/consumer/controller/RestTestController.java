package cn.lxking.consumer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RefreshScope
@RestController
public class RestTestController {

  private final RestTemplate restTemplate;
  @Value("${user.name}")
  private String userName;

  @Autowired
  public RestTestController(RestTemplate restTemplate) {
    this.restTemplate = restTemplate;
  }

  @GetMapping(value = "/rest/echo/{str}")
  public String echo(@PathVariable String str) {
    // 使用服务名请求服务提供者
    return restTemplate.getForObject("http://service-provider/echo/" + str, String.class);
  }

  @GetMapping(value = "/rest/lb")
  public String lb() {
    // 使用服务名请求服务提供者
    return restTemplate.getForObject("http://service-provider/lb", String.class);
  }

  @GetMapping(value = "/rest/config")
  public String config() {
    return restTemplate.getForObject("http://service-provider/echo/" + userName, String.class);
  }
}
