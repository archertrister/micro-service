package cn.lxking.provider.feign.base.client.service;

import cn.lxking.core.configuration.FeignRequestConfiguration;
import cn.lxking.provider.feign.base.client.service.fallback.EchoServiceFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author ：ArcherTrister
 * @date ：Created in 2020/2/27 13:31
 * @description：feign服务提供
 * @modified By：
 * @version: 1.0.0
 */
@FeignClient(value = "provider-base", path = "", configuration = FeignRequestConfiguration.class, fallback = EchoServiceFallback.class)
public interface EchoService {
    /**
     * 打印消息
     *
     * @param message {@code String} 消息
     * @return {@code String} JSON
     */
    @GetMapping(value = "echo/{message}")
    String echo(@PathVariable String message);
}
