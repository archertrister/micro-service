package cn.lxking.provider.feign.base.client.service.fallback;

import cn.lxking.core.data.ResponseResult;
import cn.lxking.core.utils.JsonUtil;
import cn.lxking.provider.feign.base.client.service.EchoService;
import org.springframework.stereotype.Component;

/**
 * @author ：ArcherTrister
 * @date ：Created in 2020/2/27 13:41
 * @description：Echo服务熔断器
 * @modified By：
 * @version: 1.0.0
 */
@Component
public class EchoServiceFallback implements EchoService {
    public static final String BREAKING_MESSAGE = "请求失败了，请检查您的网络";

    @Override
    public String echo(String message) {
        try {
            return JsonUtil.obj2json(new ResponseResult<Void>(ResponseResult.CodeStatus.BREAKING, BREAKING_MESSAGE));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
