package cn.lxking.provider.feign.base.server.controller.fallback;

import cn.lxking.core.data.ResponseResult;
import cn.lxking.provider.feign.base.client.service.fallback.EchoServiceFallback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author ：ArcherTrister
 * @date ：Created in 2020/2/27 13:55
 * @description：Echo服务熔断器
 * @modified By：
 * @version: 1.0.0
 */
public class EchoControllerFallback {
    private static final Logger logger = LoggerFactory.getLogger(EchoControllerFallback.class);

    /**
     * 熔断方法
     *
     * @param message {@code String} 消息
     * @return {@link ResponseResult}
     */
    public static ResponseResult echoFallback(String message, Throwable ex) {
        logger.warn("Invoke infoFallback: " + ex.getClass().getTypeName());
        ex.printStackTrace();
        return new ResponseResult(ResponseResult.CodeStatus.BREAKING, EchoServiceFallback.BREAKING_MESSAGE);
    }
}
