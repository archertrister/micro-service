package cn.lxking.provider.feign.base.server.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.context.properties.ConfigurationProperties;
//import org.springframework.boot.jdbc.DataSourceBuilder;
//import org.springframework.context.annotation.Bean;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.security.oauth2.provider.token.RemoteTokenServices;
//import org.springframework.security.oauth2.provider.token.TokenStore;
//import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
//import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
//import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
//import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
//import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
//
//import javax.sql.DataSource;
//import java.util.Objects;

/**
 * @author ：ArcherTrister
 * @date ：Created in 2020/2/27 14:16
 * @description：Base资源服务器配置
 * @modified By：
 * @version: 1.0.0
 */
@Configuration
@EnableResourceServer
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true, jsr250Enabled = true)
public class BaseResourceServerConfiguration extends ResourceServerConfigurerAdapter {

//    @Bean
//    public BCryptPasswordEncoder passwordEncoder() {
//        return new BCryptPasswordEncoder();
//    }
//
//    @Bean
//    @ConfigurationProperties(prefix="spring.datasource")
//    public DataSource ouathDataSource(){return DataSourceBuilder.create().build();}
//
//    @Override
//    public void configure(ResourceServerSecurityConfigurer resources)throws Exception {
//        // 配置资源 ID
//        TokenStore tokenStore=new JdbcTokenStore(ouathDataSource());
//        resources.resourceId("base-resources").tokenStore(tokenStore);
//    }

//    @Autowired(required = false)
//    private RemoteTokenServices remoteTokenServices;
//
//    @Autowired
//    private OAuth2ClientProperties oAuth2ClientProperties;
//
//    @Override
//    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
//        super.configure(resources);
//        resources.resourceId(oAuth2ClientProperties.getClientId());
//        if (Objects.nonNull(remoteTokenServices)) {
//            resources.tokenServices(remoteTokenServices);
//        }
//    }

        @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        resources.resourceId("base-resources");
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
                .exceptionHandling()
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
                .and()
                .authorizeRequests()
                .antMatchers("/**").hasAuthority("USER");
    }
}
