package cn.lxking.provider.feign.base.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author ：ArcherTrister
 * @date ：Created in 2020/2/27 11:51
 * @description：Feign服务提供
 * @modified By：
 * @version: 1.0.0
 */
@SpringBootApplication
@EnableDiscoveryClient
public class FeignBaseServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(FeignBaseServerApplication.class, args);
    }

}