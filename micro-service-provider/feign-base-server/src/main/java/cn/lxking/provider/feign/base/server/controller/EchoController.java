package cn.lxking.provider.feign.base.server.controller;

import cn.lxking.core.data.ResponseResult;
import cn.lxking.provider.feign.base.server.controller.fallback.EchoControllerFallback;
import com.alibaba.csp.sentinel.annotation.SentinelResource;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ：ArcherTrister
 * @date ：Created in 2020/2/27 13:54
 * @description：Echo api
 * @modified By：
 * @version: 1.0.0
 */
@RestController
public class EchoController {
//    @Reference(version = "1.0.0")
//    private UmsAdminService umsAdminService;

//    /**
//     * 获取个人信息
//     *
//     * @param username {@code String} 用户名
//     * @return {@link ResponseResult}
//     */
//    @GetMapping(value = "info/{username}")
//    @SentinelResource(value = "info", fallback = "echoFallback", fallbackClass = EchoControllerFallback.class)
//    public ResponseResult<UmsAdminDTO> info(@PathVariable String username) {
//        UmsAdmin umsAdmin = umsAdminService.get(username);
//        UmsAdminDTO dto = new UmsAdminDTO();
//        BeanUtils.copyProperties(umsAdmin, dto);
//        return new ResponseResult<UmsAdminDTO>(ResponseResult.CodeStatus.OK, "获取个人信息", dto);
//    }

    /**
     * 打印消息
     * 
     * @param message {@code String} 消息
     * @return {@link ResponseResult}
     */
    @GetMapping(value = "echo/{message}")
    @SentinelResource(value = "echo", fallback = "echoFallback", fallbackClass = EchoControllerFallback.class)
    public ResponseResult echo(@PathVariable String message){
        return new ResponseResult<>(ResponseResult.CodeStatus.OK, message, null);
    }
}
