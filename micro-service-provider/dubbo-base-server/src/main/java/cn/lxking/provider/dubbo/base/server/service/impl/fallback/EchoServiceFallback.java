package cn.lxking.provider.dubbo.base.server.service.impl.fallback;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author ：ArcherTrister
 * @date ：Created in 2020/2/27 11:31
 * @description：Echo服务提供者熔断器
 * @modified By：
 * @version: 1.0.0
 */
public class EchoServiceFallback {
    private static final Logger logger = LoggerFactory.getLogger(EchoServiceFallback.class);

    /**
     * 熔断方法
     *
     * @param message {@code String} 消息
     * @param ex       {@code Throwable} 异常信息
     * @return {@link String} 熔断后的固定结果
     */
    public static String echoFallback(String message, Throwable ex) {
        logger.warn("Invoke getByUsernameFallback: " + ex.getClass().getTypeName());
        ex.printStackTrace();
        return null;
    }
}
