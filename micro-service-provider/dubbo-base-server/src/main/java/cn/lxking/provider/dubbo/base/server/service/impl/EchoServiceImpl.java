package cn.lxking.provider.dubbo.base.server.service.impl;

import cn.lxking.provider.dubbo.base.client.service.EchoService;
import cn.lxking.provider.dubbo.base.server.service.impl.fallback.EchoServiceFallback;
import com.alibaba.csp.sentinel.annotation.SentinelResource;
import org.apache.dubbo.config.annotation.Service;

/**
 * @author ArcherTrister
 */
@Service(version = "1.0.0")
public class EchoServiceImpl implements EchoService {
  /**
   * 熔断器的使用
   *
   * <p>
   * 1.  {@link SentinelResource#value()} 对应的是 Sentinel 控制台中的资源，可用作控制台设置【流控】和【降级】操作 <br>
   * 2.  {@link SentinelResource#fallback()} 对应的是 {@link EchoServiceFallback#echoFallback(String, Throwable)}，并且必须为 `static` <br>
   * 3. 如果不设置 {@link SentinelResource#fallbackClass()}，则需要在当前类中创建一个 `Fallback` 函数，函数签名与原函数一致或加一个 {@link Throwable} 类型的参数
   * </p>
   *
   * @param message {@code String} 消息
   * @return {@link String}
   */
  @Override
  @SentinelResource(value = "getByUsername", fallback = "getByUsernameFallback", fallbackClass = EchoServiceFallback.class)
  public String echo(String message) {
    return "Echo Hello Dubbo " + message;
  }
}
