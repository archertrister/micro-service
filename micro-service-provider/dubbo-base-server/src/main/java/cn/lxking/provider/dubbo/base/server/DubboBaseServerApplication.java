package cn.lxking.provider.dubbo.base.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/** @author ArcherTrister */
@SpringBootApplication
// @MapperScan(basePackages = "cn.lxking.provider.base.server.mapper")
public class DubboBaseServerApplication {
  public static void main(String[] args) {
    SpringApplication.run(DubboBaseServerApplication.class, args);
  }
}
