package cn.lxking.provider.dubbo.base.client.service;

public interface EchoService {
  String echo(String string);
}
